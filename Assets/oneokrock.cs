﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class oneokrock : MonoBehaviour
{

    private string inputNum;
    private int a;
    private int n;
    private int cnt;
    public Text judgment;
    public GameObject Text1;
    public GameObject Text2;
    public GameObject Text3;
    public Text Text4;

    public GameObject button1;
    public GameObject button2;

    void Start()
    {
        a = Random.Range(1, 101); // 1から101の間で乱数を発生させる。
        print("正解の数字  " + a);
    }

    void Update()
    {

    }


    public void button()
    {
        // （ポイント）スクリプトは「InputField」につける。
        // InputFieldのtextプロパティから入力値を取得する。
        inputNum = this.gameObject.GetComponent<InputField>().text;

        // 入力値は「string型」なのでこれを「int型」に変換する。
        n = int.Parse(inputNum);
        Debug.Log("入力した数字  " + n);

        Text1.SetActive(false);
        Text2.SetActive(false);
        Text3.SetActive(false);

        cnt++;       //カウント加算
        Debug.Log(cnt);
        Text4.text = cnt.ToString();

        if (n == a)
        {
            print("正解");
            //judgment.text = "正解";
            Text1.SetActive(true);
            button1.SetActive(false);
            button2.SetActive(true);
        }
        else if (n > a)
        {
            print("大きい");
            //judgment.text = "大きいです";
            Text2.SetActive(true);

        }
        else if (n < a)
        {
            print("小さい");
            //judgment.text = "小さいです";
            Text3.SetActive(true);
        }
    }

    public void Button2()
    {
        cnt = 0;
        SceneManager.LoadScene("SampleScene");
    }
}
